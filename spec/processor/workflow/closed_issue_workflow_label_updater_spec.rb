# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/workflow/closed_issue_workflow_label_updater'

RSpec.describe Triage::Workflow::ClosedIssueWorkflowLabelUpdater do
  include_context 'with event', 'Triage::IssuableEvent' do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        action: 'close',
        from_gitlab_org?: from_gitlab_org,
        label_names: label_names,
        event_actor_username: 'user'
      }
    end
  end

  let(:from_gitlab_org) { true }
  let(:label_names)     { ['workflow::verification'] }

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.close']

  describe '#applicable?' do
    include_examples 'applicable on contextual event'

    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when no labels are set' do
      let(:label_names) { [] }

      include_examples 'event is not applicable'
    end

    context 'when "workflow::verification" is set' do
      let(:label_names) { [Labels::WORKFLOW_VERIFICATION] }

      include_examples 'event is applicable'
    end

    context 'when "workflow::complete" is set' do
      let(:label_names) { [Labels::WORKFLOW_COMPLETE] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment' do
      body = <<~MARKDOWN.chomp
        @user, the workflow label was automatically updated to ~"workflow::complete" because you closed the issue while in ~"workflow::verification".

        If this is not the correct label, please update.

        To avoid this message, update the workflow label as you close the issue.
        /label ~"workflow::complete"
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
