# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../lib/constants/labels'
require_relative '../../../triage/processor/compliance_group/assign_dev_for_verification'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::AssignComplianceDevForVerification do
  include_context 'with event', 'Triage::IssueEvent' do
    let(:label_names) { ['workflow::verification', 'group::compliance', 'backend'] }
    let(:project_id) { 12 }
    let(:issue_iid) { 1234 }
    let(:event_attrs) do
      {
        project_id: project_id,
        iid: issue_iid,
        from_gitlab_org?: true,
        assignee_ids: [],
        label_names: label_names
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.update']

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event project is not a part of gitlab org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when group and workflow labels are not in label_names' do
      before do
        allow(event).to receive(:label_names).and_return([])
      end

      include_examples 'event is not applicable'
    end

    context 'when issue is assigned' do
      before do
        allow(event).to receive(:assignee_ids).and_return([1])
      end

      include_examples 'event is not applicable'
    end

    context 'when labels are not valid' do
      before do
        allow(event).to receive(:label_names).and_return(["workflow::refinement"])
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:compliance_be_members) { %w[@compliance_be_1 @compliance_be_2] }
    let(:compliance_fe_members) { %w[@compliance_fe_1 @compliance_fe_2] }

    shared_examples 'processes event' do |assigned_username, unassigned_username: nil|
      before do
        allow(subject).to receive(:sleep)

        allow(subject).to receive(:compliance_be).with(except: unassigned_username) do
          (compliance_be_members - ["@#{unassigned_username}"]).first
        end

        allow(subject).to receive(:compliance_fe).with(except: unassigned_username) do
          (compliance_fe_members - ["@#{unassigned_username}"]).first
        end
      end

      let(:issue) do
        {
          project_id: project_id,
          iid: issue_iid,
          assignees: [],
          state: 'opened'
        }
      end

      let(:notes) do
        if unassigned_username
          [{ created_at: "2013-10-02T09:22:45Z",
             "body" => "unassigned @#{unassigned_username}" }]
        else
          []
        end
      end

      it 'posts a comment' do
        body = <<~MARKDOWN.chomp
          This issue is ready to be verified and according to our [verification process](https://about.gitlab.com/handbook/engineering/development/sec/govern/compliance/#verification)
          we need your help with this activity.

          #{assigned_username}, would you mind taking a look if this issue can be verified on production and close this issue?

          /assign #{assigned_username}
        MARKDOWN

        expect_api_requests do |requests|
          requests << stub_api_request(path: "/projects/#{project_id}/issues/#{issue_iid}", response_body: issue)
          requests << stub_api_request(path: "/projects/#{project_id}/issues/#{issue_iid}/notes", response_body: notes)
          requests << stub_comment_request(event: event, body: body)

          subject.process
        end
      end
    end

    context 'when issue belongs to compliance frontend' do
      let(:label_names) { ['workflow::verification', Labels::COMPLIANCE_GROUP_LABEL, Labels::FRONTEND_LABEL] }

      it_behaves_like 'processes event', '@compliance_fe_1'
      it_behaves_like 'processes event', '@compliance_fe_2', unassigned_username: 'compliance_fe_1'
    end

    context 'when issue belongs to compliance backend' do
      let(:label_names) { ['workflow::verification', Labels::COMPLIANCE_GROUP_LABEL, Labels::BACKEND_LABEL] }

      it_behaves_like 'processes event', '@compliance_be_1'
      it_behaves_like 'processes event', '@compliance_be_2', unassigned_username: 'compliance_be_1'
    end

    context 'when group label is different' do
      let(:label_names) { ['workflow::verification', 'group::composition analysis', 'backend'] }

      it 'raises exception' do
        expect { subject.process }.to raise_error(described_class::InvalidLabelsError)
      end
    end

    context 'when there is assignee' do
      let(:issue) do
        {
          project_id: project_id,
          iid: issue_iid,
          assignees: [{ id: 1 }],
          state: 'opened'
        }
      end

      it 'does not assign dev' do
        expect_api_request(path: "/projects/#{project_id}/issues/#{issue_iid}", response_body: issue) do
          subject.process
        end
      end
    end

    context 'when issue is closed' do
      let(:issue) do
        {
          project_id: project_id,
          iid: issue_iid,
          state: 'closed'
        }
      end

      it 'does not assign dev' do
        expect_api_request(path: "/projects/#{project_id}/issues/#{issue_iid}", response_body: issue) do
          subject.process
        end
      end
    end
  end
end
