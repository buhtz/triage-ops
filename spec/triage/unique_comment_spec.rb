# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/event'
require_relative '../../triage/triage/processor'
require_relative '../../triage/triage/unique_comment'

RSpec.describe Triage::UniqueComment do
  include_context 'with event' do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        action: 'open'
      }
    end
  end

  subject { described_class.new('namespace::TestClass', event) }

  describe 'NOTES_PER_PAGE' do
    it { expect(described_class::NOTES_PER_PAGE).to eq(100) }
  end

  describe '#wrap' do
    it { expect(subject.wrap("Hello World!")).to eq("<!-- triage-serverless TestClass -->\nHello World!") }
  end

  describe '#no_previous_comment?' do
    let(:note_author) { 'someone' }

    def expect_notes_request_with(event, body:, &block)
      response_body =
        if body
          [{ body: body, author: { username: note_author } }]
        else
          []
        end

      expect_api_request(path: "/projects/#{event.project_id}/#{event.object_kind}s/#{event.iid}/notes", query: { per_page: 100 }, response_body: response_body, &block)
    end

    %w[Issue MergeRequest].each do |resource_type|
      context "with a #{resource_type} event" do
        include_context 'with event', "Triage::#{resource_type}Event" do
          let(:event_attrs) do
            {
              object_kind: resource_type.gsub(/(?<lowercase_letter>[a-z])(?<uppercase_letter>[A-Z])/, '\k<lowercase_letter>_\k<uppercase_letter>').downcase,
              issue?: resource_type == "Issue",
              merge_request?: resource_type == "MergeRequest",
              project_id: 42,
              iid: 12
            }
          end
        end

        context "when there is no previous comment" do
          it "returns true" do
            expect_notes_request_with(event, body: nil) do
              expect(subject.no_previous_comment?).to be(true)
            end
          end
        end

        context "when there is a previous comment" do
          let(:note) do
            "<!-- triage-serverless TestClass -->\nHello World!"
          end

          it "returns false" do
            expect_notes_request_with(event, body: note) do
              expect(subject.no_previous_comment?).to be(false)
            end
          end

          context "when the marker is not at the beginning of the line" do
            it "returns false" do
              expect_notes_request_with(event, body: "There can be another marker at the beginning!\n#{note}") do
                expect(subject.no_previous_comment?).to be(false)
              end
            end
          end

          context "when we specify who this comment should be from" do
            subject do
              described_class.new('namespace::TestClass', event, from: expected_author)
            end

            context "when it is from the expected user" do
              let(:expected_author) { note_author }

              it "returns false" do
                expect_notes_request_with(event, body: note) do
                  expect(subject.no_previous_comment?).to be(false)
                end
              end
            end

            context "when it is not from the expected user" do
              let(:expected_author) { note_author.swapcase }

              it "returns true" do
                expect_notes_request_with(event, body: note) do
                  expect(subject.no_previous_comment?).to be(true)
                end
              end
            end

            context "when we specific a unique comment identifier different from a previous comment" do
              subject do
                described_class.new('namespace::TestClass', event, 'string 1234, not TestClass!')
              end

              it "returns true" do
                expect_notes_request_with(event, body: note) do
                  expect(subject.no_previous_comment?).to be(true)
                end
              end
            end

            context "when we specify a unique comment identifier same as a previous comment" do
              subject do
                described_class.new('namespace::TestClass', event, 'TestClass')
              end

              it "returns false" do
                expect_notes_request_with(event, body: note) do
                  expect(subject.no_previous_comment?).to be(false)
                end
              end
            end
          end
        end
      end
    end
  end

  describe '#previous_discussion' do
    it 'returns a note that matches the hidden comment' do
      note = double(Gitlab::ObjectifiedHash, body: '<!-- triage-serverless TestClass -->')
      discussion = { 'notes' => [note] }
      allow(subject).to receive(:resource_discussions).and_return([discussion])

      expect(subject.previous_discussion).to eq(discussion)
    end
  end

  describe '#previous_discussion_comment' do
    context 'when a previous discussion is found' do
      before do
        note = double(Gitlab::ObjectifiedHash, body: '<!-- triage-serverless TestClass --> a comment')
        discussion = { 'notes' => [note] }

        allow(subject).to receive(:resource_discussions).and_return([discussion])
      end

      it 'returns the discussion comment' do
        expect(subject.previous_discussion_comment.body).to eq('<!-- triage-serverless TestClass --> a comment')
      end
    end

    context 'when a previous discussion is not found' do
      before do
        note = double(Gitlab::ObjectifiedHash, body: 'a note')
        discussion = { 'notes' => [note] }

        allow(subject).to receive(:resource_discussions).and_return([discussion])
      end

      it 'returns nil' do
        expect(subject.previous_discussion_comment).to be_nil
      end
    end
  end
end
