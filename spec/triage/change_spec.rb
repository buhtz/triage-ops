# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/change'

RSpec.describe Triage::Change do
  let(:changed_file_list) { Triage::ChangedFileList.new(project_id, merge_request_iid) }

  let(:project_id) { 42 }
  let(:merge_request_iid) { 12 }

  let(:model_change) do
    {
      'old_path' => 'app/models/user.rb',
      'new_path' => 'app/models/user.rb',
      'diff' => '+ model'
    }
  end

  let(:doc_change) do
    {
      'old_path' => 'doc/user.md',
      'new_path' => 'doc/user.md',
      'diff' => '+ doc'
    }
  end

  let(:merge_request_changes) do
    {
      'changes' => [model_change, doc_change]
    }
  end

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
  end

  describe '.detect' do
    context 'when file_patterns matches nothing' do
      before do
        allow(described_class).to receive(:file_patterns).and_return('nothing')
      end

      it 'collects nothing' do
        expect(described_class.detect(changed_file_list)).to eq([])
      end
    end

    context 'when file_patterns matches doc change' do
      before do
        allow(described_class).to receive(:file_patterns).and_return(%r{\Adoc/})
      end

      context 'when line_patterns matches nothing' do
        before do
          allow(described_class).to receive(:line_patterns).and_return(/nothing/)
        end

        it 'collects nothing' do
          expect(described_class.detect(changed_file_list)).to eq([])
        end
      end

      context 'when line_patterns matches changes' do
        before do
          allow(described_class).to receive(:line_patterns).and_return(/doc/)
        end

        it 'collects doc change' do
          expect(described_class.detect(changed_file_list)).to eq([described_class.new(doc_change)])
        end
      end
    end
  end

  describe '.file_patterns' do
    it 'raises NotImplementedError' do
      expect { described_class.file_patterns }.to raise_error(NotImplementedError)
    end
  end

  describe '.line_patterns' do
    it 'raises NotImplementedError' do
      expect { described_class.line_patterns }.to raise_error(NotImplementedError)
    end
  end

  describe '#==' do
    it 'returns false for other objects' do
      expect(described_class.new({})).not_to eq({})
    end

    it 'returns false for holding different change' do
      detected_doc = described_class.new(doc_change)
      detected_model = described_class.new(model_change)

      expect(detected_doc).not_to eq(detected_model)
    end

    it 'returns true for holding the same change' do
      detected_doc = described_class.new(doc_change)
      detected_doc_again = described_class.new(doc_change)

      expect(detected_doc).to eq(detected_doc_again)
    end

    it 'returns true for even when changes are wrapped' do
      detected_doc = described_class.new(Gitlab::ObjectifiedHash.new(doc_change))
      detected_doc_again = described_class.new(Gitlab::ObjectifiedHash.new(doc_change))

      expect(detected_doc).to eq(detected_doc_again)
    end
  end

  describe '#path' do
    it 'returns new_path from the change' do
      expect(described_class.new(doc_change).path).to eq(doc_change['new_path'])
    end
  end

  describe '#type' do
    it 'returns the class name without namespace' do
      expect(described_class.new(doc_change).type).to eq('Change')
    end
  end
end
