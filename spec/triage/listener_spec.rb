# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/listener'

describe Triage::Listener do
  subject { described_class.new('issue', 'open') }

  describe '.listeners_for_event' do
    %w[issue incident merge_request].each do |resource|
      it "returns all #{resource} listeners for (#{resource}, *) pair" do
        resource_action_pairs = for_resource_action_pairs(resource, '*')

        expected_pairs = [resource].product(
          described_class::ALLOWED_EVENT_DEFS[resource])

        expect(resource_action_pairs).to match_array(expected_pairs)
      end
    end

    it 'returns all open listeners for (*, open) pair' do
      resource_action_pairs = for_resource_action_pairs('*', 'open')

      expected_pairs = [%w[issue open], %w[incident open], %w[merge_request open]]

      expect(resource_action_pairs).to match_array(expected_pairs)
    end

    it 'returns all listeners for (*, *) pair' do
      resource_action_pairs = for_resource_action_pairs('*', '*')

      expected_pairs = %w[issue incident merge_request].flat_map do |resource|
        [resource].product(described_class::ALLOWED_EVENT_DEFS[resource])
      end

      expect(resource_action_pairs).to match_array(expected_pairs)
    end

    it 'returns the specific listener for (issue, close) pair' do
      resource_action_pairs = for_resource_action_pairs('issue', 'close')

      expected_pairs = [%w[issue close]]

      expect(resource_action_pairs).to match_array(expected_pairs)
    end

    def for_resource_action_pairs(resource, action)
      listeners = described_class.listeners_for_event(resource, action)
      listeners.map { |l| [l.resource, l.action] }
    end
  end

  describe '#initialize' do
    it 'raises ListenerResourceNotAllowedError when resource is not allowed' do
      expect { described_class.new('foo', 'open') }.to raise_error(described_class::ListenerResourceNotAllowedError)
    end

    it 'raises ListenerActionNotAllowedError when resource is not allowed' do
      expect { described_class.new('issue', 'foo') }.to raise_error(described_class::ListenerActionNotAllowedError)
    end
  end

  describe '#event' do
    specify { expect(subject.event).to eq('issue.open') }
  end
end
