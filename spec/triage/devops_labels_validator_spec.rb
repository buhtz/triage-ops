# frozen_string_literal: true

require 'spec_helper'
require_relative '../../triage/triage/devops_labels_validator'
require_relative '../../triage/resources/issue'

RSpec.describe Triage::DevopsLabelsValidator do
  include_context 'with event', 'Triage::IssueEvent' do
    let(:label_names) { [] }

    let(:event_attrs) do
      { label_names: label_names }
    end

    let(:issue_attr) do
      { 'iid' => iid, 'labels' => label_names }
    end

    let(:issue_path) { "/projects/#{project_id}/issues/#{iid}" }
    let(:issue)      { Triage::Issue.new(issue_attr) }
  end

  let(:www_gitlab_com_categories) do
    {
      'projects' => {
        'name' => 'Projects',
        'stage' => 'data_stores',
        'group' => 'pods',
        'label' => 'Category:Projects',
        'feature_labels' => []
      },
      'experimentation_adoption' => {
        'name' => 'Adoption Experiment',
        'stage' => 'growth',
        'group' => 'activation',
        'label' => 'Category:Adoption Experiment',
        'feature_labels' => []
      },
      'runner' => {
        'name' => 'GitLab Runner Core',
        'stage' => 'verify',
        'group' => 'runner',
        'label' => 'Category:Runner Core',
        'feature_labels' => []
      }
    }
  end

  subject { described_class.new(event) }

  before do
    stub_api_request(path: issue_path, response_body: issue_attr)

    stub_request(:get, "https://about.gitlab.com/categories.json")
      .to_return(status: 200, body: read_fixture('categories.json'), headers: {})
  end

  describe 'labels_set?' do
    context 'when issue has no label' do
      let(:label_names) { [] }

      it 'returns false' do
        expect(subject.labels_set?).to be false
      end
    end

    context 'when issue has section label' do
      let(:label_names) { ['section::growth'] }

      it 'returns false' do
        expect(subject.labels_set?).to be false
      end
    end

    context 'when issue has group label' do
      let(:label_names) { ['group::runner'] }

      it 'returns true' do
        expect(subject.labels_set?).to be true
      end
    end

    context 'when issue has category label' do
      let(:label_names) { ['Category:Projects'] }

      it 'returns true' do
        expect(subject.labels_set?).to be true
      end
    end
  end
end
