# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/generate/group_policy'

RSpec.describe Generate::GroupPolicy do
  let(:options) { double(:options, template: template_path, only: only) }
  let(:only) { nil }
  let(:template_path) { 'path/to/template' }
  let(:template_name) { File.basename(template_path) }
  let(:output_dir) { "#{described_class.destination}/#{template_name}" }
  let(:fake_group) { 'fake' }
  let(:fake_group_definition) { {} }

  before do
    expect(File)
      .to receive(:read)
      .with(template_path)
      .and_return(<<~ERB)
        <%= group_method_name %>
        <%= group_label_name %>
      ERB

    expect(FileUtils)
      .to receive(:rm_r)

    expect(FileUtils)
      .to receive(:mkdir_p)
      .with(output_dir)

    stub_const('GroupDefinition::DATA', fake_group => fake_group_definition)
  end

  describe '.run' do
    let(:fake_group) { 'editor' }

    shared_examples 'generates and writes policy files' do
      it 'generates' do
        expect(GroupDefinition)
          .to receive(:public_send)
          .with("group_#{fake_group}")
          .and_return({ labels: ["group::#{fake_group}"] })

        expect(File)
          .to receive(:write)
          .with(
            "#{output_dir}/#{fake_group}.yml",
            <<~MARKDOWN)
              group_#{fake_group}
              group::#{fake_group}
            MARKDOWN

        described_class.run(options)
      end
    end

    it_behaves_like 'generates and writes policy files'

    context 'when --only is provided with the group' do
      let(:only) { ['editor'] }

      it_behaves_like 'generates and writes policy files'
    end

    context 'when --only is provided with another group' do
      let(:only) { ['source code'] }

      it 'does not generate anything' do
        expect(GroupDefinition)
          .not_to receive(:public_send)

        expect(File)
          .not_to receive(:write)

        described_class.run(options)
      end
    end

    context 'when the group has a matching ignore_templates' do
      let(:fake_group_definition) do
        { 'ignore_templates' => [template_path] }
      end

      it 'generates nothing' do
        expect(File).not_to receive(:write)

        described_class.run(options)
      end
    end
  end
end
