# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/www_gitlab_com'
require_relative '../../lib/team_member_helper'

RSpec.describe TeamMemberHelper do
  let(:resource_klass) do
    Class.new do
      include TeamMemberHelper
    end
  end

  let(:team_from_www) { { 'user' => { 'specialty' => ['Foo'] } } }

  subject { resource_klass.new }

  before do
    allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)
  end

  describe '#has_specialty?' do
    context 'when user is a team member' do
      context 'when user has the required specialty' do
        let(:team_from_www) { { 'user' => { 'specialty' => 'Foo' } } }

        it 'returns true' do
          expect(subject).to have_specialty('user', 'Foo')
        end
      end

      context 'when user has many specialties including the required specialty' do
        let(:team_from_www) { { 'user' => { 'specialty' => %w[Bar Foo Baz] } } }

        it 'returns true' do
          expect(subject).to have_specialty('user', 'Foo')
        end
      end

      context 'when user does not have the required specialty' do
        let(:team_from_www) { { 'user' => { 'specialty' => 'Bar' } } }

        it 'returns false' do
          expect(subject).not_to have_specialty('user', 'Foo')
        end
      end

      context 'when user has many specialties but not the required specialty' do
        let(:team_from_www) { { 'user' => { 'specialty' => %w[Bar Baz] } } }

        it 'returns false' do
          expect(subject).not_to have_specialty('user', 'Foo')
        end
      end

      context 'when user has no specialty' do
        let(:team_from_www) { { 'user' => {} } }

        it 'returns false' do
          expect(subject).not_to have_specialty('user', 'Foo')
        end
      end
    end

    context 'when user is not a team member' do
      let(:team_from_www) { { 'another_user' => { 'specialty' => 'Foo' } } }

      it 'returns false' do
        expect(subject).not_to have_specialty('user', 'Foo')
      end
    end
  end
end
