# frozen_string_literal: true
require_relative '../triage'
require_relative '../triage/event'
require_relative '../resources/issue'

module Triage
  class BrokenMasterIncidentLabelValidator
    UNDETERMINED_ROOT_CAUSE_LABEL = 'master-broken::undetermined'
    MASTER_BROKEN_FLAKY_TEST_LABEL = 'master-broken::flaky-test'
    FLAKY_TEST_LABEL_PREFIX = 'flaky-test::'
    ROOT_CAUSE_LABEL_PREFIX = 'master-broken::'

    attr_reader :event

    def initialize(event)
      @event = event
    end

    def need_root_cause_label?
      issue.labels.include?(UNDETERMINED_ROOT_CAUSE_LABEL) || !has_root_cause_label?
    end

    def need_flaky_reason_label?
      issue.labels.include?(MASTER_BROKEN_FLAKY_TEST_LABEL) && !has_flaky_reason_label?
    end

    private

    def has_flaky_reason_label?
      issue.labels.any? { |label| label.start_with? FLAKY_TEST_LABEL_PREFIX }
    end

    def has_root_cause_label?
      issue.labels.any? { |label| label.start_with? ROOT_CAUSE_LABEL_PREFIX }
    end

    def issue
      @issue ||= Triage::Issue.new(Triage.api_client.issue(event.project_id, event.iid).to_h)
    end
  end
end
