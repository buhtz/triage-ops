# frozen_string_literal: true

require_relative 'versioned_milestone'
require 'date'

module MissedResourceHelper
  GITLAB_RELEASE_DATE = 22

  def missed_resource?(milestone_due_date, release_date = nil, current_date = today)
    return false unless milestone_due_date
    return false if current_date < milestone_due_date

    release_date ||= release_date_for(milestone_due_date)

    current_date >= missed_resource_date_for(release_date)
  end

  def add_missed_labels(milestone_title, labels)
    result = [
      %(/label ~"missed:#{milestone_title}")
    ]

    result << '/label ~"missed-deliverable"' if labels.index('Deliverable')

    result.join("\n")
  end

  def move_to_current_milestone
    current_milestone_title = VersionedMilestone.new(self).current.title

    %(/milestone %"#{current_milestone_title}")
  end

  private

  def missed_resource_date_for(release_date)
    return release_date - 3 if release_date.monday? # delivery date on Friday
    return release_date - 2 if release_date.sunday? # delivery date on Friday

    release_date - 1 # delivery date on previous day
  end

  def release_date_for(due_date)
    Date.new(due_date.year, due_date.month, GITLAB_RELEASE_DATE)
  end

  def today
    if fake_today = ENV['TRIAGE_FAKE_TODAY_FOR_MISSED_RESOURCES']
      Date.parse(fake_today)
    else
      Date.today
    end
  end
end
