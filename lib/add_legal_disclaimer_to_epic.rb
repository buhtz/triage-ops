# frozen_string_literal: true

require_relative '../triage/triage'

class AddLegalDisclaimerToEpic
  LEGAL_DISCLAIMER_VERSION = 3
  LEGAL_DISCLAIMER_TAG = "<!-- triage-serverless v#{LEGAL_DISCLAIMER_VERSION} PLEASE DO NOT REMOVE THIS SECTION -->".freeze
  LEGAL_DISCLAIMER_TAG_REGEX = /#{Regexp.escape(LEGAL_DISCLAIMER_TAG).sub(/\\ v#{LEGAL_DISCLAIMER_VERSION}/, '( v)?(?<version>\d+)?')}/
  LEGAL_DISCLAIMER_REGEX = /#{LEGAL_DISCLAIMER_TAG_REGEX}\n(?<disclaimer_text>.+)\n#{LEGAL_DISCLAIMER_TAG_REGEX}/m
  LEGAL_DISCLAIMER_TEXT = <<~MESSAGE
    *This page may contain information related to upcoming products, features and functionality.
    It is important to note that the information presented is for informational purposes only, so please do not rely on the information for purchasing or planning purposes.
    Just like with all projects, the items mentioned on the page are subject to change or delay, and the development, release, and timing of any products, features, or functionality remain at the sole discretion of GitLab Inc.*
  MESSAGE
  LEGAL_DISCLAIMER = "#{LEGAL_DISCLAIMER_TAG}\n#{LEGAL_DISCLAIMER_TEXT}#{LEGAL_DISCLAIMER_TAG}".freeze

  def initialize(epic_object:, resource:)
    network_options = epic_object.__send__(:network).options
    @dry_run = network_options.dry_run
    @resource = resource
    @api_client = Gitlab.client(endpoint: Triage::PRODUCTION_API_ENDPOINT, private_token: network_options.token)
  end

  def process
    return if description_includes_latest_legal_disclaimer?

    if description_includes_any_legal_disclaimer?
      update_legal_disclaimer
    else
      add_legal_disclaimer
    end
  end

  private

  attr_reader :dry_run, :resource, :api_client

  def description_includes_any_legal_disclaimer?
    LEGAL_DISCLAIMER_REGEX.match?(resource_description)
  end

  def description_includes_latest_legal_disclaimer?
    LEGAL_DISCLAIMER_VERSION == current_legal_disclaimer_version
  end

  def current_legal_disclaimer_version
    matches = LEGAL_DISCLAIMER_REGEX.match(resource_description)
    return unless matches

    matches[:version].to_i
  end

  def resource_description
    resource[:description]
  end

  def update_legal_disclaimer
    edit_epic_description(resource_description.gsub(LEGAL_DISCLAIMER_REGEX, LEGAL_DISCLAIMER))
  end

  def add_legal_disclaimer
    edit_epic_description("#{resource_description}\n#{LEGAL_DISCLAIMER}")
  end

  def edit_epic_description(new_description)
    if dry_run
      puts "\n[DRY RUN] New description for epic #{resource.dig(:references, :full)} (description truncated for clarity):\n\n#{new_description.slice(0, LEGAL_DISCLAIMER.size + 100)}\n\n---------------------------"
    else
      api_client.edit_epic(resource[:group_id], resource[:iid], description: new_description)
    end
  end
end
